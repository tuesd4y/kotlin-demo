package org.bitbucket.tuesd4y.kotlindemo

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

/**
 * @author Christopher Stelzmüller
 * @version 1.0
 * created on 01.12.2016
 */

public class RecyclerViewAdapter(private val ctx: Context, public var items: MutableList<Item>) : RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(ctx).inflate(R.layout.simple_list_item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(items[position])
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val id: TextView
        private val name: TextView
        private val date: TextView

        init {
            id = itemView.findViewById(R.id.tv_name) as TextView
            name = itemView.findViewById(R.id.tv_description) as TextView
            date = itemView.findViewById(R.id.tv_owner_name) as TextView
        }

        public fun bindItem(item: Item){
            id.text = item.id.toString()
            name.text = item.name
            date.text = item.created.toString()
        }

    }
}