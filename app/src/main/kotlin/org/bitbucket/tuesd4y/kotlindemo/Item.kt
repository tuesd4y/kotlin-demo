package org.bitbucket.tuesd4y.kotlindemo

import org.joda.time.LocalDate

/**
 * @author Christopher Stelzmüller
 * @version 1.0
 * created on 02.12.2016
 */

data class Item( val id: Int, val name: String, val created: LocalDate)