package org.bitbucket.tuesd4y.kotlindemo

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager

import kotlinx.android.synthetic.main.activity_main.*

/**
 * @author Christopher Stelzmüller
 * @version 1.0
 * created on 01.12.2016
 */

class MainActivity : AppCompatActivity() {

    lateinit var adapter: RecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rv_items.setHasFixedSize(true)
        rv_items.layoutManager = LinearLayoutManager(this)
        rv_items.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))

        adapter = RecyclerViewAdapter(this, Data.items)
        rv_items.adapter = adapter

        btn_sort_name.setOnClickListener {
            adapter.items.sortBy{item -> item.name}
            adapter.notifyDataSetChanged()
        }

        btn_sort_id.setOnClickListener {
            adapter.items.sortBy (Item::id)
            adapter.notifyDataSetChanged()
        }

        btn_sort_created.setOnClickListener {
            adapter.items.sortBy { it.created }
            adapter.notifyDataSetChanged()
        }

        tiet_search_text.addTextChangedListener(afterTextChanged {
            val text: String = it.toString()
            adapter.items = Data.items.filter {
                item -> item.name.contains(text, ignoreCase = true)
            } as MutableList<Item>
            adapter.notifyDataSetChanged()
        })
    }

    companion object {

    }

    fun afterTextChanged(callback: (CharSequence) -> Unit) : AfterTextEditWatcher{
        return AfterTextEditWatcher(callback)
    }
}
