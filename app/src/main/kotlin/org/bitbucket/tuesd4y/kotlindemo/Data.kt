package org.bitbucket.tuesd4y.kotlindemo

import org.joda.time.LocalDate

/**
 * @author Christopher Stelzmüller
 * @version 1.0
 * created on 01.12.2016
 */

object Data {
    val items =  mutableListOf(
            Item(0, "Toaster", LocalDate.now().minusYears(5)),
            Item(1, "Stove", LocalDate.now().minusYears(2)),
            Item(2, "Cupboard", LocalDate.now().minusYears(3)),
            Item(3, "Phone", LocalDate.now().minusMonths(10)),
            Item(4, "Table", LocalDate.now().minusMonths(23)),
            Item(5, "Music Player", LocalDate.now().minusWeeks(3))
    )
}