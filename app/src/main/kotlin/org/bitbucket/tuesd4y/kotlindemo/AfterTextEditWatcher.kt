package org.bitbucket.tuesd4y.kotlindemo

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher

/**
 * @author Christopher Stelzmüller
 * @version 1.0
 * created on 01.12.2016
 */

class AfterTextEditWatcher(val callback: (CharSequence) -> Unit) : TextWatcher {

    override fun afterTextChanged(text: Editable) {}

    override fun beforeTextChanged(text: CharSequence, p1: Int, p2: Int, p3: Int) {}

    override fun onTextChanged(text: CharSequence, p1: Int, p2: Int, p3: Int) {
        callback(text)
    }
}