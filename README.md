# Kotlin Android demo 

## What is this? ##
This is a small project that shows how to simplify android development by using kotlin.

## Links ##
* [Presentation](http://slides.com/tuesd4y/kotlin-for-android#/view)
* [Kotlin Cheatsheet](https://gist.github.com/dodyg/5823184)
* [LINQ-like Syntax examples](https://github.com/mythz/kotlin-linq-examples)
* [Kotlin General](https://medium.com/@octskyward/kotlin-fp-3bf63a17d64a#.7nxe6mslc)
* [Kotlin Syntactic Sugar](https://kotlinlang.org/docs/reference/idioms.html)

## Author ##
_Christopher Stelzmüller_